create database if not exists test;
use test;
select * from notebook;
#drop database test;


CREATE TABLE IF NOT EXISTS instituicao (
    instituicao_id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nome VARCHAR(80) NOT NULL,
    rsocial VARCHAR(60) NOT NULL,
    cnpj INT NOT NULL,
    logradouro VARCHAR(60) NOT NULL,
    cep CHAR(10) NOT NULL,
    cidade VARCHAR(30) NOT NULL,
    estado VARCHAR(30) NOT NULL,
    uf CHAR(2) NOT NULL
);

CREATE TABLE IF NOT EXISTS pessoa (
    id_pessoa INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nome VARCHAR(60) NOT NULL,
    cpf CHAR(16) NOT NULL,
    matricula CHAR(7) NOT NULL,
    professor CHAR(4) NOT NULL,
    instituicao_id INT NOT NULL,
    FOREIGN KEY (instituicao_id)
        REFERENCES instituicao (instituicao_id)
);

CREATE TABLE IF NOT EXISTS notebook (
	id_notebook INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    codigo_notebook CHAR(7) NOT NULL,
    descricao TEXT,
    especificacao TEXT,
    instituicao_id INT NOT NULL,
    FOREIGN KEY (instituicao_id) REFERENCES instituicao (instituicao_id)
);

