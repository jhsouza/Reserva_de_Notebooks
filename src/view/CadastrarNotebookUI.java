package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;

import model.Instituicao;
import model.Notebook;

import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.JToolBar;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;

import controller.InstituicaoController;
import controller.NotebookController;
import javax.swing.JScrollPane;

public class CadastrarNotebookUI extends JInternalFrame {
	private JTextField jtfCodPatrimonio;
	private JTextArea textAreaDescricao;
	private JTextArea textAreaEspecificacao;
	private Notebook notebookParaEdicao;
	JComboBox<Instituicao> combobInstituicao;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarNotebookUI frame = new CadastrarNotebookUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastrarNotebookUI() {
		setTitle("Cadastro de notebook");
		setBounds(100, 100, 512, 553);
		setLocation(200, 40);
		
		JPanel jpCadastroNotebook = new JPanel();
		jpCadastroNotebook.setBorder(new TitledBorder(null, "Dados do Notebook", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PrincipalUI.getInstance().ativaBotoes(3);
				dispose();
			}
		});
		
		
		JLabel lblCodigoPatrimonio = new JLabel("C\u00F3digo de Patrim\u00F4nio:");
		
		jtfCodPatrimonio = new JTextField();
		jtfCodPatrimonio.setColumns(10);
		
		JLabel lblDescricao = new JLabel("Descri\u00E7\u00E3o:");
		
		JLabel lblEspecificacao = new JLabel("Especifica\u00E7\u00F5es:");
		
		JLabel lblInstituicao = new JLabel("Institui\u00E7\u00E3o:");
		
		
		
		DefaultComboBoxModel<Instituicao> modelInst = new DefaultComboBoxModel<>();
		for (Instituicao inst : new InstituicaoController().listarTodos()) {
			modelInst.addElement(inst);
		}
		combobInstituicao = new JComboBox<>();
		combobInstituicao.setModel(modelInst);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Instituicao inst = (Instituicao) combobInstituicao.getSelectedItem();
				try {
					if (notebookParaEdicao == null) {
						Notebook notebook = new Notebook();
						notebook.setCodigo(jtfCodPatrimonio.getText());
						notebook.setDescricao(textAreaDescricao.getText());
						notebook.setSpecs(textAreaEspecificacao.getText());	
						notebook.setInstituicao(inst);
						new NotebookController().salvar(notebook);
					} else {
						notebookParaEdicao.setCodigo(jtfCodPatrimonio.getText());
						notebookParaEdicao.setDescricao(textAreaDescricao.getText());
						notebookParaEdicao.setSpecs(textAreaEspecificacao.getText());	
						notebookParaEdicao.setInstituicao(inst);
						new NotebookController().editar(notebookParaEdicao);
					}
					PrincipalUI.getInstance().ativaBotoes(3);
					dispose();
				} catch (Exception e){
					JOptionPane.showMessageDialog(null, e.getMessage());
				}								
			}
		});
		
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(jpCadastroNotebook, GroupLayout.PREFERRED_SIZE, 476, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnSalvar)
							.addGap(18)
							.addComponent(btnCancelar)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jpCadastroNotebook, GroupLayout.PREFERRED_SIZE, 449, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnSalvar)
						.addComponent(btnCancelar))
					.addContainerGap(171, Short.MAX_VALUE))
		);
		
		JScrollPane srpAreaDescricao = new JScrollPane();
		srpAreaDescricao.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		JScrollPane srpAreaEspecificacao = new JScrollPane();
		srpAreaEspecificacao.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		GroupLayout gl_jpCadastroNotebook = new GroupLayout(jpCadastroNotebook);
		gl_jpCadastroNotebook.setHorizontalGroup(
			gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jpCadastroNotebook.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_jpCadastroNotebook.createSequentialGroup()
							.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.TRAILING)
									.addComponent(lblCodigoPatrimonio)
									.addComponent(lblInstituicao))
								.addComponent(lblDescricao))
							.addGap(18)
							.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
								.addComponent(combobInstituicao, 0, 324, Short.MAX_VALUE)
								.addComponent(jtfCodPatrimonio, GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)))
						.addComponent(lblEspecificacao)
						.addComponent(srpAreaDescricao, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE)
						.addComponent(srpAreaEspecificacao, GroupLayout.PREFERRED_SIZE, 446, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_jpCadastroNotebook.setVerticalGroup(
			gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jpCadastroNotebook.createSequentialGroup()
					.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.BASELINE)
						.addComponent(jtfCodPatrimonio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCodigoPatrimonio))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.BASELINE)
						.addComponent(combobInstituicao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblInstituicao))
					.addGap(10)
					.addComponent(lblDescricao)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(srpAreaDescricao, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEspecificacao)
					.addPreferredGap(ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
					.addComponent(srpAreaEspecificacao, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		textAreaEspecificacao = new JTextArea();
		srpAreaEspecificacao.setViewportView(textAreaEspecificacao);
		textAreaEspecificacao.setLineWrap(true);
		textAreaEspecificacao.setWrapStyleWord(true);
		
		//Cria JTextArea e faz com que a quebra de linha ocorra (porque isso nao � padr�o???)
		textAreaDescricao = new JTextArea();
		srpAreaDescricao.setViewportView(textAreaDescricao);
		textAreaDescricao.setLineWrap(true);
		textAreaDescricao.setWrapStyleWord(true);
		jpCadastroNotebook.setLayout(gl_jpCadastroNotebook);
		getContentPane().setLayout(groupLayout);

	}
	
	public Notebook getNotebookParaEdicao() {
		return notebookParaEdicao;
	}

	public void setNotebookParaEdicao(Notebook notebookParaEdicao) {
		this.notebookParaEdicao = notebookParaEdicao;
		preencherCamposParaEdicao();
	}
	
	public void preencherCamposParaEdicao() {
		if (notebookParaEdicao != null) {
			jtfCodPatrimonio.setText("" + notebookParaEdicao.getCodigo());
			textAreaDescricao.setText("" + notebookParaEdicao.getDescricao());
			textAreaEspecificacao.setText("" + notebookParaEdicao.getSpecs());
		}
	}
	
}
