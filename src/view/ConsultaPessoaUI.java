package view;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controller.PessoaController;
import dao.PessoaDAO;
import interfaceController.IPessoaController;
import model.Pessoa;
import model.PessoaTableModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.border.EtchedBorder;

import java.awt.Color;

public class ConsultaPessoaUI extends JInternalFrame {
	private JTextField jtfPesquisa;
	private JTable jtListaPessoas;
	private PessoaController pessoaControl = new PessoaController(); 
	private JButton btnNovaPessoa;
	private static ConsultaPessoaUI instancia;

	/**
	 * Launch the application.
	 */
	
	public static ConsultaPessoaUI getInstance() {
		if (instancia == null) {
			instancia = new ConsultaPessoaUI();
		}
		return instancia;
	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaPessoaUI frame = getInstance();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsultaPessoaUI() {
		setTitle("Consulta de Alunos/Professores");
		setBounds(100, 100, 520, 314);
		setLocation(200, 40);
		
		JPanel jpPesquisa = new JPanel();
		jpPesquisa.setBorder(new TitledBorder(new EtchedBorder(
				EtchedBorder.LOWERED, null, null),
				"Pesquisa de Aluno/Professor", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));

		JScrollPane jspTabelaPessoas = new JScrollPane();

		btnNovaPessoa = new JButton("Novo Aluno/Professor");
		btnNovaPessoa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(PrincipalUI.getInstance().getBtnCadastrarPessoa().isEnabled()){
					CadastrarPessoaUI clienteUI = new CadastrarPessoaUI();
					getParent().add(clienteUI, 0);
					clienteUI.setVisible(true);
					PrincipalUI.getInstance().desativaBotoes(5);
				}else{
					btnNovaPessoa.setEnabled(false);	
				}
			}
		});

		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int linhaSelecionada = jtListaPessoas.getSelectedRow();
				try {

					Pessoa pessoa = new PessoaTableModel(PessoaDAO
							.obterInstancia().listaPessoas)
							.get(linhaSelecionada);
					CadastrarPessoaUI pessoaUI = new CadastrarPessoaUI();
					pessoaUI.setPessoaParaEdicao(pessoa);
					getParent().add(pessoaUI, 0);
					pessoaUI.setVisible(true);

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Selecione um Aluno/Professor");
				}

			}

		});

		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				int linhaSelecionada = jtListaPessoas.getSelectedRow();
				Pessoa pessoa = new PessoaTableModel(
						PessoaDAO.obterInstancia().listaPessoas)
						.get(linhaSelecionada);
				new PessoaController().remover(pessoa);

				List<Pessoa> listaPessoas = pessoaControl.listarTodos();
				PessoaTableModel modelPessoa = new PessoaTableModel(
						listaPessoas);
				jtListaPessoas.setModel(modelPessoa);
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, "Selecione um Aluno/Professor");
				}
			}
		});

		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PrincipalUI.getInstance().ativaBotoes(6);
				dispose();
			}
		});

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				btnNovaPessoa)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				btnEditar)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				btnExcluir)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				btnFechar))
														.addComponent(
																jspTabelaPessoas,
																GroupLayout.DEFAULT_SIZE,
																481,
																Short.MAX_VALUE)
														.addComponent(
																jpPesquisa, 0,
																0,
																Short.MAX_VALUE))
										.addContainerGap(23, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addContainerGap()
								.addComponent(jpPesquisa,
										GroupLayout.PREFERRED_SIZE, 71,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(jspTabelaPessoas,
										GroupLayout.PREFERRED_SIZE, 151,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(
										groupLayout
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(btnNovaPessoa)
												.addComponent(btnEditar)
												.addComponent(btnExcluir)
												.addComponent(btnFechar))
								.addContainerGap(15, Short.MAX_VALUE)));

		jtListaPessoas = new JTable();
		PessoaTableModel modelCliente = new PessoaTableModel(
				new PessoaController().listarTodos());
		jtListaPessoas.setModel(modelCliente);
		jspTabelaPessoas.setViewportView(jtListaPessoas);

		jtfPesquisa = new JTextField();
		jtfPesquisa.setColumns(10);

		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				List<Pessoa> filtro = new PessoaController().pesquisar(jtfPesquisa.getText());
				PessoaTableModel modelPessoa = new PessoaTableModel(filtro);
				jtListaPessoas.setModel(modelPessoa);
				
			}
		});
		GroupLayout gl_jpPesquisa = new GroupLayout(jpPesquisa);
		gl_jpPesquisa.setHorizontalGroup(gl_jpPesquisa.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_jpPesquisa
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(jtfPesquisa, GroupLayout.PREFERRED_SIZE,
								349, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnPesquisar)
						.addContainerGap(36, Short.MAX_VALUE)));
		gl_jpPesquisa
				.setVerticalGroup(gl_jpPesquisa
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_jpPesquisa
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_jpPesquisa
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																jtfPesquisa,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																btnPesquisar))
										.addContainerGap(56, Short.MAX_VALUE)));
		jpPesquisa.setLayout(gl_jpPesquisa);
		getContentPane().setLayout(groupLayout);

	}


	
	
}
