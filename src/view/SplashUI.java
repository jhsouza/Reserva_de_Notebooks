package view;
import ferramentas.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.JProgressBar;

public class SplashUI extends JFrame {

	private JPanel contentPane;
	static JProgressBar barra = new JProgressBar();
	FerramentasFrame ferramenta = new FerramentasFrame();
	PrincipalUI p = new PrincipalUI();

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SplashUI frame = new SplashUI();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		});
	}

	public SplashUI() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 680);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(SplashUI.class.getResource("/img/Splash_Logo.fw.png")));

		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(SplashUI.class.getResource("/img/Splash_Nome.fw.png")));

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING).addGroup(gl_contentPane
				.createSequentialGroup()
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup().addContainerGap().addComponent(lblNewLabel))
						.addGroup(gl_contentPane.createSequentialGroup().addGap(54).addComponent(lblNewLabel_1))
						.addGroup(gl_contentPane.createSequentialGroup().addGap(106).addComponent(barra,
								GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap(14, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane
				.createSequentialGroup()
				.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(lblNewLabel_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(barra, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE).addGap(5)));
		barra.setStringPainted(true);
		contentPane.setLayout(gl_contentPane);

		ferramenta.centralizarFrame(this);
		ferramenta.aplicarTransparencia(this);
		

		// Esta Thread � usada para carregar a barra e frear o loop de carregamento
		

		new Thread() {

			public void run() {

				for (int i = 0; i <= 100; i++) {

					try {
						sleep(0); //Basta alterar este valor para 28 (n�o mais do que isso!!) para que o Spash seja visivel
						barra.setValue(i);
					} catch (InterruptedException e) {

						e.printStackTrace();
					}

				}

				if (barra.getValue() == 100) {

					dispose();
					p.setVisible(true);
					

				}

			}

		}.start();

	}

}
