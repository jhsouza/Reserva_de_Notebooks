package view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;

import controller.InstituicaoController;
import controller.NotebookController;
import model.Instituicao;
import model.Notebook;
import javax.swing.JScrollPane;

public class VisualizaNotebookUI extends JInternalFrame {
	private JTextField jtfCodPatrimonio;
	private JTextArea textAreaDescricao;
	private JTextArea textAreaEspecificacao;
	private Notebook notebookParaEdicao;
	JComboBox<Instituicao> combobInstituicao;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VisualizaNotebookUI frame = new VisualizaNotebookUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VisualizaNotebookUI() {
		setTitle("Visualizar Notebook");
		setBounds(100, 100, 512, 553);
		setLocation(200, 40);
		
		JPanel jpCadastroNotebook = new JPanel();
		jpCadastroNotebook.setBorder(new TitledBorder(null, "Dados do Notebook", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		
		
		JButton btnCancelar = new JButton("Fechar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PrincipalUI.getInstance().ativaBotoes(3);
				dispose();
			}
		});
		
		
		JLabel lblCodigoPatrimonio = new JLabel("C\u00F3digo de Patrim\u00F4nio:");
		
		jtfCodPatrimonio = new JTextField();
		jtfCodPatrimonio.setColumns(10);
		
		JLabel lblDescricao = new JLabel("Descri\u00E7\u00E3o:");
		
		JLabel lblEspecificacao = new JLabel("Especifica\u00E7\u00F5es:");
		
		JLabel lblInstituicao = new JLabel("Institui\u00E7\u00E3o:");
		
		
		
		DefaultComboBoxModel<Instituicao> modelInst = new DefaultComboBoxModel<>();
		for (Instituicao inst : new InstituicaoController().listarTodos()) {
			modelInst.addElement(inst);
		}
		combobInstituicao = new JComboBox<>();
		combobInstituicao.setModel(modelInst);
		
		
		
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(jpCadastroNotebook, GroupLayout.PREFERRED_SIZE, 476, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(btnCancelar)))
					.addGap(10))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jpCadastroNotebook, GroupLayout.PREFERRED_SIZE, 449, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCancelar)
					.addContainerGap(171, Short.MAX_VALUE))
		);
		
		JScrollPane srpAreaDescricao = new JScrollPane();
		//faz com que barra vertical apare�a sempre
		srpAreaDescricao.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		JScrollPane srpAreaEspecificacao = new JScrollPane();
		srpAreaEspecificacao.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		GroupLayout gl_jpCadastroNotebook = new GroupLayout(jpCadastroNotebook);
		gl_jpCadastroNotebook.setHorizontalGroup(
			gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jpCadastroNotebook.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_jpCadastroNotebook.createSequentialGroup()
							.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.TRAILING)
									.addComponent(lblCodigoPatrimonio)
									.addComponent(lblInstituicao))
								.addComponent(lblDescricao))
							.addGap(18)
							.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
								.addComponent(combobInstituicao, 0, 324, Short.MAX_VALUE)
								.addComponent(jtfCodPatrimonio, GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)))
						.addComponent(lblEspecificacao)
						.addComponent(srpAreaDescricao, GroupLayout.PREFERRED_SIZE, 444, GroupLayout.PREFERRED_SIZE)
						.addComponent(srpAreaEspecificacao, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_jpCadastroNotebook.setVerticalGroup(
			gl_jpCadastroNotebook.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jpCadastroNotebook.createSequentialGroup()
					.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.BASELINE)
						.addComponent(jtfCodPatrimonio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCodigoPatrimonio))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_jpCadastroNotebook.createParallelGroup(Alignment.BASELINE)
						.addComponent(combobInstituicao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblInstituicao))
					.addGap(10)
					.addComponent(lblDescricao)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(srpAreaDescricao, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEspecificacao)
					.addPreferredGap(ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
					.addComponent(srpAreaEspecificacao, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE))
		);
		
		textAreaEspecificacao = new JTextArea();
		srpAreaEspecificacao.setViewportView(textAreaEspecificacao);
		textAreaEspecificacao.setLineWrap(true);
		textAreaEspecificacao.setWrapStyleWord(true);
		
		//Cria JTextArea e faz com que a quebra de linha ocorra (porque isso nao � padr�o???)
		textAreaDescricao = new JTextArea();
		srpAreaDescricao.setViewportView(textAreaDescricao);
		textAreaDescricao.setLineWrap(true);
		textAreaDescricao.setWrapStyleWord(true);
		jpCadastroNotebook.setLayout(gl_jpCadastroNotebook);
		getContentPane().setLayout(groupLayout);

	}
	
	public Notebook getNotebookParaEdicao() {
		return notebookParaEdicao;
	}

	public void setNotebookParaEdicao(Notebook notebookParaEdicao) {
		this.notebookParaEdicao = notebookParaEdicao;
		preencherCamposParaEdicao();
	}
	
	public void preencherCamposParaEdicao() {
		if (notebookParaEdicao != null) {
			jtfCodPatrimonio.setText("" + notebookParaEdicao.getCodigo());
			textAreaDescricao.setText("" + notebookParaEdicao.getDescricao());
			textAreaEspecificacao.setText("" + notebookParaEdicao.getSpecs());
		}
	}
}
