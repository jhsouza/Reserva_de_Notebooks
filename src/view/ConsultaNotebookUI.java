package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import controller.InstituicaoController;
import controller.NotebookController;
import dao.InstituicaoDAO;
import dao.NotebookDAO;
import model.Instituicao;
import model.InstituicaoTableModel;
import model.Notebook;
import model.NotebookTableModel;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;

public class ConsultaNotebookUI extends JInternalFrame {
	private JTable jtListarNotebooks;
	private NotebookController notebookControl = new NotebookController();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaNotebookUI frame = new ConsultaNotebookUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsultaNotebookUI() {
		setBounds(100, 100, 657, 551);
		
		JPanel jpConsultaNotebook = new JPanel();
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_jpConsultaNotebook = new GroupLayout(jpConsultaNotebook);
		gl_jpConsultaNotebook.setHorizontalGroup(
			gl_jpConsultaNotebook.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 641, Short.MAX_VALUE)
		);
		gl_jpConsultaNotebook.setVerticalGroup(
			gl_jpConsultaNotebook.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 449, Short.MAX_VALUE)
		);
		
		jtListarNotebooks = new JTable();
		NotebookTableModel modelNotebook = new NotebookTableModel(new NotebookController().listarTodos());
		scrollPane.setViewportView(jtListarNotebooks);
		jtListarNotebooks.setModel(modelNotebook);
		scrollPane.setViewportView(jtListarNotebooks);
		
		
		JButton btnNovoNotebook = new JButton("Novo Notebook");
		btnNovoNotebook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastrarNotebookUI novoNotebook = new CadastrarNotebookUI();
				getParent().add(novoNotebook, 0);
				novoNotebook.setVisible(true);
			}
		});
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int linhaSelecionada = jtListarNotebooks.getSelectedRow();
				Notebook notebook = new NotebookTableModel(NotebookDAO.obterInstancia().listaNotebooks).get(linhaSelecionada);
				CadastrarNotebookUI noteUI = new CadastrarNotebookUI();
				noteUI.setNotebookParaEdicao(notebook);
				getParent().add(noteUI, 0);
				noteUI.setVisible(true);
			}
		});
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int linhaSelecionada = jtListarNotebooks.getSelectedRow();
				try {
					Notebook notebook = new NotebookTableModel(NotebookDAO.obterInstancia().listarNotebook()).get(linhaSelecionada);
					new NotebookController().remover(notebook);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "Selecione um notebook");
				}

				List<Notebook> listaNotebook = notebookControl.listarTodos();
				NotebookTableModel modelNotebook = new NotebookTableModel(listaNotebook);
				jtListarNotebooks.setModel(modelNotebook);
			}
		});
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PrincipalUI.getInstance().ativaBotoes(4);
				dispose();
			}
		});
		
		JButton btnVisualizarNotebook = new JButton("Visualizar");
		btnVisualizarNotebook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//seleciona a linha
				int linhaSelecionada = jtListarNotebooks.getSelectedRow();
				//instancia o notebook para pegar as infos da linha selecionada conforme o id
				Notebook notebook = new NotebookTableModel(NotebookDAO.obterInstancia().listaNotebooks).get(linhaSelecionada);
				VisualizaNotebookUI visuUI = new VisualizaNotebookUI();
				visuUI.setNotebookParaEdicao(notebook);
				getParent().add(visuUI,0);
				visuUI.setVisible(true);
			}
		});
		
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(jpConsultaNotebook, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 641, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnNovoNotebook)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnVisualizarNotebook)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnEditar)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnExcluir)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnFechar)
					.addContainerGap(249, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(jpConsultaNotebook, GroupLayout.PREFERRED_SIZE, 449, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNovoNotebook)
						.addComponent(btnFechar)
						.addComponent(btnExcluir)
						.addComponent(btnEditar)
						.addComponent(btnVisualizarNotebook))
					.addContainerGap(31, Short.MAX_VALUE))
		);
		
		jpConsultaNotebook.setLayout(gl_jpConsultaNotebook);
		getContentPane().setLayout(groupLayout);
	}
}
