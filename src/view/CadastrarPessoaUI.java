package view;

import java.awt.EventQueue;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import controller.InstituicaoController;
import controller.PessoaController;
import model.Instituicao;
import model.Pessoa;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.border.EtchedBorder;

import java.awt.Color;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;

public class CadastrarPessoaUI extends JInternalFrame {
	private JTextField jtfNome;
	private JTextField jtfCpf;
	private JTextField jtfMatricula;
	private JCheckBox checkbProfessor;
	private Pessoa pessoaParaEdicao;
	JComboBox<Instituicao> combobInstituicao;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarPessoaUI frame = new CadastrarPessoaUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastrarPessoaUI() {
		setTitle("Cadastro de Aluno/Pessoa");
		setBounds(100, 100, 500, 275);
		setLocation(200, 40);

		checkbProfessor = new JCheckBox("Professor");
		JPanel jpCadastroPessoa = new JPanel();
		jpCadastroPessoa.setBorder(new TitledBorder(new EtchedBorder(
				EtchedBorder.LOWERED, null, null), "Dados do Aluno/Professor",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));

		JButton btnSalvar = new JButton("Salvar");

		DefaultComboBoxModel<Instituicao> modelInst = new DefaultComboBoxModel<>();
		for (Instituicao inst : new InstituicaoController().listarTodos()) {
			modelInst.addElement(inst);
		}

		combobInstituicao = new JComboBox<>();
		combobInstituicao.setModel(modelInst);

		btnSalvar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				Instituicao inst = (Instituicao) combobInstituicao
						.getSelectedItem();
				try {
					if (pessoaParaEdicao == null) {
						Pessoa pessoa = new Pessoa();
						pessoa.setNome(jtfNome.getText());
						pessoa.setCpf(jtfCpf.getText());
						pessoa.setMatricula(jtfMatricula.getText());
						if (checkbProfessor.isSelected()) {
							pessoa.setProfessor("sim");
						} else {
							pessoa.setProfessor("nao");
						}
						pessoa.setInstituicao(inst);
						new PessoaController().salvar(pessoa);

					} else {
						pessoaParaEdicao.setNome(jtfNome.getText());
						pessoaParaEdicao.setCpf(jtfCpf.getText());
						pessoaParaEdicao.setMatricula(jtfMatricula.getText());
						if (checkbProfessor.isSelected()) {
							pessoaParaEdicao.setProfessor("sim");
						} else {
							pessoaParaEdicao.setProfessor("nao");
						}
						pessoaParaEdicao.setInstituicao(inst);

						new PessoaController().editar(pessoaParaEdicao);

					}
					PrincipalUI.getInstance().ativaBotoes(5);
					dispose();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		});

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PrincipalUI.getInstance().ativaBotoes(5);
				dispose();
			}
		});

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(9)
																		.addComponent(
																				btnSalvar)
																		.addGap(5)
																		.addComponent(
																				btnCancelar))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				jpCadastroPessoa,
																				GroupLayout.DEFAULT_SIZE,
																				474,
																				Short.MAX_VALUE)))
										.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						Alignment.TRAILING,
						groupLayout
								.createSequentialGroup()
								.addContainerGap()
								.addComponent(jpCadastroPessoa,
										GroupLayout.PREFERRED_SIZE, 191,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, 9,
										Short.MAX_VALUE)
								.addGroup(
										groupLayout
												.createParallelGroup(
														Alignment.LEADING)
												.addComponent(btnSalvar,
														Alignment.TRAILING)
												.addComponent(btnCancelar,
														Alignment.TRAILING))
								.addGap(15)));

		JLabel lblNome = new JLabel("Nome:");

		jtfNome = new JTextField();
		jtfNome.setColumns(10);

		JLabel lblCpf = new JLabel("CPF:");

		jtfCpf = new JTextField();
		jtfCpf.setColumns(10);

		JLabel lblMatricula = new JLabel("Matricula:");

		jtfMatricula = new JTextField();
		jtfMatricula.setColumns(10);

		JLabel lblInstituicao = new JLabel("Institui\u00E7\u00E3o");

		GroupLayout gl_jpCadastroPessoa = new GroupLayout(jpCadastroPessoa);
		gl_jpCadastroPessoa
				.setHorizontalGroup(gl_jpCadastroPessoa
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_jpCadastroPessoa
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_jpCadastroPessoa
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_jpCadastroPessoa
																		.createSequentialGroup()
																		.addGroup(
																				gl_jpCadastroPessoa
																						.createParallelGroup(
																								Alignment.LEADING,
																								false)
																						.addComponent(
																								lblNome)
																						.addComponent(
																								jtfNome,
																								GroupLayout.DEFAULT_SIZE,
																								414,
																								Short.MAX_VALUE)
																						.addGroup(
																								gl_jpCadastroPessoa
																										.createSequentialGroup()
																										.addComponent(
																												lblCpf)
																										.addGap(159)
																										.addComponent(
																												lblMatricula))
																						.addGroup(
																								gl_jpCadastroPessoa
																										.createSequentialGroup()
																										.addComponent(
																												jtfCpf,
																												GroupLayout.PREFERRED_SIZE,
																												164,
																												GroupLayout.PREFERRED_SIZE)
																										.addGap(18)
																										.addComponent(
																												jtfMatricula)))
																		.addContainerGap(
																				38,
																				Short.MAX_VALUE))
														.addGroup(
																Alignment.TRAILING,
																gl_jpCadastroPessoa
																		.createSequentialGroup()
																		.addGroup(
																				gl_jpCadastroPessoa
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblInstituicao)
																						.addComponent(
																								combobInstituicao,
																								0,
																								340,
																								Short.MAX_VALUE))
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				checkbProfessor)
																		.addGap(35)))));
		gl_jpCadastroPessoa
				.setVerticalGroup(gl_jpCadastroPessoa
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_jpCadastroPessoa
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(lblNome)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(jtfNome,
												GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_jpCadastroPessoa
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblCpf)
														.addComponent(
																lblMatricula))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_jpCadastroPessoa
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																jtfCpf,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jtfMatricula,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(lblInstituicao)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_jpCadastroPessoa
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																combobInstituicao,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																checkbProfessor))
										.addContainerGap(24, Short.MAX_VALUE)));
		jpCadastroPessoa.setLayout(gl_jpCadastroPessoa);
		getContentPane().setLayout(groupLayout);

	}

	public Pessoa getPessoaParaEdicao() {
		return pessoaParaEdicao;
	}

	public void setPessoaParaEdicao(Pessoa pessoaParaEdicao) {
		this.pessoaParaEdicao = pessoaParaEdicao;
		preencherCamposParaEdicao();
	}

	public void preencherCamposParaEdicao() {
		if (pessoaParaEdicao != null) {
			jtfNome.setText(pessoaParaEdicao.getNome());
			jtfCpf.setText(pessoaParaEdicao.getCpf());
			jtfMatricula.setText(pessoaParaEdicao.getMatricula());
			if (pessoaParaEdicao.getProfessor().equals("sim")) {
				checkbProfessor.setSelected(true);
			} else {
				checkbProfessor.setSelected(false);
			}

		}
	}
}
