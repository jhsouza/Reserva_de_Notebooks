package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JInternalFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.SwingConstants;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import controller.InstituicaoController;
import model.Instituicao;
import model.Pessoa;

import javax.swing.border.EtchedBorder;

import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JCheckBox;

public class CadastrarInstituicaoUI extends JInternalFrame {

	private JTextField jtfNome;
	private JTextField jtfRsocial;
	private JTextField jtfCep;
	private JTextField jtflogradouro;
	private JTextField jtfCidade;
	private JTextField jtfEstado;
	private JTextField jtfUf;
	private JTextField jtfCnpj;
	private Instituicao instituicaoParaEdicao;

	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarInstituicaoUI frame = new CadastrarInstituicaoUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CadastrarInstituicaoUI() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("Cadastro de Instituição");
		setBounds(100, 100, 500, 344);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(200, 40);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null),
				"Nova Institui\u00E7\u00E3o", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		JLabel label = new JLabel("Nome:");

		jtfNome = new JTextField();
		jtfNome.setColumns(10);

		JLabel lblCnpj = new JLabel("Raz\u00E3o Social");

		JLabel lblEndereo = new JLabel("CEP");

		jtfRsocial = new JTextField();
		jtfRsocial.setColumns(10);

		jtfCep = new JTextField();
		jtfCep.setColumns(10);

		jtflogradouro = new JTextField();
		jtflogradouro.setColumns(10);

		JLabel lblRua = new JLabel("Logradouro");

		JLabel lblCidade = new JLabel("Cidade");

		jtfCidade = new JTextField();
		jtfCidade.setColumns(10);

		JLabel lblEstado = new JLabel("Estado");

		jtfEstado = new JTextField();
		jtfEstado.setColumns(10);

		JLabel lblUf = new JLabel("UF");

		jtfUf = new JTextField();
		jtfUf.setColumns(10);

		jtfCnpj = new JTextField();
		jtfCnpj.setColumns(10);

		JLabel label_1 = new JLabel("CNPJ");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addContainerGap()
						.addGroup(gl_panel
								.createParallelGroup(Alignment.LEADING).addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
												.addComponent(jtfNome, GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
												.addGroup(gl_panel.createSequentialGroup()
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																.addComponent(lblCnpj).addComponent(jtfRsocial,
																		GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE))
														.addPreferredGap(ComponentPlacement.RELATED)
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																.addComponent(label_1, GroupLayout.PREFERRED_SIZE,
																		33, GroupLayout.PREFERRED_SIZE)
																.addComponent(jtfCnpj, Alignment.TRAILING,
																		GroupLayout.PREFERRED_SIZE, 150,
																		GroupLayout.PREFERRED_SIZE))))
										.addGap(17))
								.addGroup(gl_panel.createSequentialGroup().addComponent(label).addContainerGap(411,
										Short.MAX_VALUE))
								.addGroup(gl_panel.createSequentialGroup().addGroup(gl_panel
										.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel.createSequentialGroup()
												.addComponent(lblCidade, GroupLayout.PREFERRED_SIZE,
														54, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED, 371, Short.MAX_VALUE))
										.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup().addGroup(gl_panel
												.createParallelGroup(Alignment.TRAILING)
												.addComponent(jtfCidade, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
														204, Short.MAX_VALUE)
												.addComponent(lblRua, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 69,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(jtflogradouro, Alignment.LEADING,
														GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
														.addGroup(gl_panel.createSequentialGroup()
																.addGroup(gl_panel
																		.createParallelGroup(Alignment.LEADING)
																		.addComponent(jtfEstado).addComponent(
																				lblEstado, GroupLayout.PREFERRED_SIZE,
																				54, GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(ComponentPlacement.RELATED)
																.addGroup(gl_panel
																		.createParallelGroup(Alignment.LEADING, false)
																		.addComponent(lblUf, GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(jtfUf, GroupLayout.PREFERRED_SIZE,
																				24, GroupLayout.PREFERRED_SIZE)))
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																.addComponent(lblEndereo).addComponent(jtfCep,
																		GroupLayout.PREFERRED_SIZE, 150,
																		GroupLayout.PREFERRED_SIZE)))))
										.addGap(17)))));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addContainerGap().addComponent(label)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(jtfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblCnpj)
								.addComponent(label_1))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(jtfRsocial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(jtfCnpj, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblRua)
												.addComponent(lblEndereo))
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(jtflogradouro,
												GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addComponent(jtfCep, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel.createSequentialGroup().addComponent(lblCidade)
												.addPreferredGap(ComponentPlacement.RELATED).addComponent(jtfCidade,
														GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
										.addGroup(gl_panel.createSequentialGroup().addComponent(lblUf)
												.addPreferredGap(ComponentPlacement.RELATED).addComponent(jtfUf,
														GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_panel.createSequentialGroup().addComponent(lblEstado)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(jtfEstado,
												GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(13, Short.MAX_VALUE)));
		panel.setLayout(gl_panel);

		JButton button = new JButton("Cancelar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PrincipalUI.getInstance().ativaBotoes(1);
				dispose();
				
			}
		});

		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				if (instituicaoParaEdicao == null) {
					Instituicao inst = new Instituicao();
					inst.setNome(jtfNome.getText());
					inst.setRsocial(jtfRsocial.getText());
					inst.setCnpj(Long.parseLong(jtfCnpj.getText()));
					inst.setLogradouro(jtflogradouro.getText());
					inst.setCep(jtfCep.getText());
					inst.setCidade(jtfCidade.getText());
					inst.setEstado(jtfEstado.getText());
					inst.setUf(jtfUf.getText());

					new InstituicaoController().salvar(inst);

				} else {
					instituicaoParaEdicao.setNome(jtfNome.getText());
					instituicaoParaEdicao.setRsocial(jtfRsocial.getText());
					instituicaoParaEdicao.setCnpj(Long.parseLong(jtfCnpj.getText()));
					instituicaoParaEdicao.setLogradouro(jtflogradouro.getText());
					instituicaoParaEdicao.setCep(jtfCep.getText());
					instituicaoParaEdicao.setCidade(jtfCidade.getText());
					instituicaoParaEdicao.setEstado(jtfEstado.getText());
					instituicaoParaEdicao.setUf(jtfUf.getText());

					new InstituicaoController().editar(instituicaoParaEdicao);

				}
				PrincipalUI.getInstance().ativaBotoes(1);
				dispose();

			}

		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING,
								groupLayout.createSequentialGroup().addContainerGap(287, Short.MAX_VALUE)
										.addComponent(btnCadastrar, GroupLayout.PREFERRED_SIZE, 92,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(button, GroupLayout.PREFERRED_SIZE, 85,
												GroupLayout.PREFERRED_SIZE)
										.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup().addContainerGap()
								.addComponent(panel, GroupLayout.PREFERRED_SIZE, 464, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		groupLayout
				.setVerticalGroup(
						groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addContainerGap()
										.addComponent(panel, GroupLayout.PREFERRED_SIZE, 231,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnCadastrar, GroupLayout.PREFERRED_SIZE, 23,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(button))
										.addContainerGap(29, Short.MAX_VALUE)));
		getContentPane().setLayout(groupLayout);

	}

	public Instituicao getInstituicaoParaEdicao() {
		return instituicaoParaEdicao;
	}

	public void setInstituicaoParaEdicao(Instituicao instituicaoParaEdicao) {
		this.instituicaoParaEdicao = instituicaoParaEdicao;
		preencherCamposParaEdicao();
	}

	public void preencherCamposParaEdicao() {
		if (instituicaoParaEdicao != null) {
			jtfNome.setText(instituicaoParaEdicao.getNome());
			jtfCnpj.setText(""+instituicaoParaEdicao.getCnpj());
			jtflogradouro.setText(instituicaoParaEdicao.getLogradouro());
			jtfCep.setText(instituicaoParaEdicao.getCep());
			jtfCidade.setText(instituicaoParaEdicao.getCidade());
			jtfEstado.setText(instituicaoParaEdicao.getEstado());
			jtfUf.setText(instituicaoParaEdicao.getUf());
			jtfRsocial.setText(instituicaoParaEdicao.getRsocial());


		}
	}
}
