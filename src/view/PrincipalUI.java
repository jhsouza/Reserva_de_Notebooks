package view;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;

import java.awt.Color;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class PrincipalUI extends JFrame {

	private JPanel contentPane;
	private static PrincipalUI instancia;
	
	private JButton btnCadastrarInstituicao;	
	private JButton btnConsultarInstituicao;	
	private JButton btnCadastrarNotebook;
	private JButton btnConsultarNotebook;	
	private JButton btnCadastrarPessoa;
	private JButton btnConsultarPessoa;
	private JTable reservaPrincipal;
	
	/**
	 * Launch the application.
	 */
	
	
	
	public static PrincipalUI getInstance() {
		if (instancia == null) {
			instancia = new PrincipalUI();
		}
		return instancia;
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalUI frame = getInstance();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrincipalUI() {
		setResizable(false);
		setTitle("Sistema de emprestimos de notebooks");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1017, 907);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel aluno_professor = new JPanel();
		aluno_professor.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Professor/Aluno", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JPanel notebook = new JPanel();
		notebook.setBorder(new TitledBorder(null, "Notebook", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		
		JPanel instituicao = new JPanel();
		instituicao.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Instituicao", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Reserva", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(instituicao, 0, 0, Short.MAX_VALUE)
						.addComponent(notebook, 0, 0, Short.MAX_VALUE)
						.addComponent(aluno_professor, GroupLayout.PREFERRED_SIZE, 159, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(14, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 441, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(aluno_professor, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(notebook, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(instituicao, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)))
					.addGap(13))
		);
		
		JButton btnReservar = new JButton("Reservar");
		
		JButton btnEtregar = new JButton("Entregar");
		
		JButton btnEditar = new JButton("Editar");
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 675, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btnReservar)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnEtregar)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnEditar)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 368, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnReservar)
						.addComponent(btnEtregar)
						.addComponent(btnEditar))
					.addContainerGap())
		);
		
		reservaPrincipal = new JTable();
		reservaPrincipal.setEnabled(false);
		reservaPrincipal.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null},
			},
			new String[] {
				"Cod. Notebook", "Reservado por"
			}
		));
		scrollPane.setViewportView(reservaPrincipal);
		panel.setLayout(gl_panel);
		
		 btnCadastrarInstituicao = new JButton("Cadastrar");
		 btnCadastrarInstituicao.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		CadastrarInstituicaoUI cadI = new CadastrarInstituicaoUI();
				contentPane.add(cadI,0);
				cadI.setVisible(true);	
				desativaBotoes(1);
		 		
		 	}
		 });
		instituicao.add(btnCadastrarInstituicao);
		
		 btnConsultarInstituicao = new JButton("Consultar");
		 btnConsultarInstituicao.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		ConsultaInstituicaoUI conI = new ConsultaInstituicaoUI();
				contentPane.add(conI,0);
				conI.setVisible(true);	
				desativaBotoes(2);
		 		
		 	}
		 });
		instituicao.add(btnConsultarInstituicao);
		
		 btnCadastrarNotebook = new JButton("Cadastrar");
		 btnCadastrarNotebook.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		CadastrarNotebookUI cadN = new CadastrarNotebookUI();
		 		contentPane.add(cadN,0);
		 		cadN.setVisible(true);
		 		desativaBotoes(3);
		 	}
		 });
		notebook.add(btnCadastrarNotebook);
		
		 btnConsultarNotebook = new JButton("Consultar");
		 btnConsultarNotebook.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		ConsultaNotebookUI conN = new ConsultaNotebookUI();
		 		contentPane.add(conN, 0);
		 		conN.setVisible(true);
		 		desativaBotoes(4);
		 	}
		 });
		notebook.add(btnConsultarNotebook);
		
		 btnCadastrarPessoa = new JButton("Cadastrar");
		btnCadastrarPessoa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastrarPessoaUI cadAP = new CadastrarPessoaUI();
				contentPane.add(cadAP,0);
				cadAP.setVisible(true);	
				desativaBotoes(5);
			}
		});
		aluno_professor.add(btnCadastrarPessoa);
		
		 btnConsultarPessoa = new JButton("Consultar");
		btnConsultarPessoa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaPessoaUI consultaAP = new ConsultaPessoaUI();
				contentPane.add(consultaAP,0);
				consultaAP.setVisible(true);
				desativaBotoes(6);
			}
		});
		aluno_professor.add(btnConsultarPessoa);
		contentPane.setLayout(gl_contentPane);
	}

	public void ativaBotoes(int i) {
		if(i==1){
			this.btnCadastrarInstituicao.setEnabled(true);
		}else if(i==2){
			this.btnConsultarInstituicao.setEnabled(true);
		}else if(i==3){
			this.btnCadastrarNotebook.setEnabled(true);
		}else if(i==4){
			this.btnConsultarNotebook.setEnabled(true);
		}else if(i==5){
			this.btnCadastrarPessoa.setEnabled(true);	
		}else if(i==6){
			this.btnConsultarPessoa.setEnabled(true);
		}else if(i==7){
			this.btnCadastrarInstituicao.setEnabled(true);
			this.btnConsultarInstituicao.setEnabled(true);
			this.btnCadastrarNotebook.setEnabled(true);
			this.btnConsultarNotebook.setEnabled(true);
			this.btnCadastrarPessoa.setEnabled(true);	
			this.btnConsultarPessoa.setEnabled(true);

		}	
	}

	public void desativaBotoes(int i) {
		if(i==1){
			this.btnCadastrarInstituicao.setEnabled(false);
		}else if(i==2){
			this.btnConsultarInstituicao.setEnabled(false);
		}else if(i==3){
			this.btnCadastrarNotebook.setEnabled(false);
		}else if(i==4){
			this.btnConsultarNotebook.setEnabled(false);
		}else if(i==5){
			this.btnCadastrarPessoa.setEnabled(false);	
		}else if(i==6){
			this.btnConsultarPessoa.setEnabled(false);

		}else if(i==7){
			this.btnCadastrarInstituicao.setEnabled(false);
			this.btnConsultarInstituicao.setEnabled(false);
			this.btnCadastrarNotebook.setEnabled(false);
			this.btnConsultarNotebook.setEnabled(false);
			this.btnCadastrarPessoa.setEnabled(false);	
			this.btnConsultarPessoa.setEnabled(false);

		}
	}

	public JButton getBtnCadastrarInstituicao() {
		return btnCadastrarInstituicao;
	}

	public void setBtnCadastrarInstituicao(JButton btnCadastrarInstituicao) {
		this.btnCadastrarInstituicao = btnCadastrarInstituicao;
	}

	public JButton getBtnConsultarInstituicao() {
		return btnConsultarInstituicao;
	}

	public void setBtnConsultarInstituicao(JButton btnConsultarInstituicao) {
		this.btnConsultarInstituicao = btnConsultarInstituicao;
	}

	public JButton getBtnCadastrarNotebook() {
		return btnCadastrarNotebook;
	}

	public void setBtnCadastrarNotebook(JButton btnCadastrarNotebook) {
		this.btnCadastrarNotebook = btnCadastrarNotebook;
	}

	public JButton getBtnConsultarNotebook() {
		return btnConsultarNotebook;
	}

	public void setBtnConsultarNotebook(JButton btnConsultarNotebook) {
		this.btnConsultarNotebook = btnConsultarNotebook;
	}

	public JButton getBtnCadastrarPessoa() {
		return btnCadastrarPessoa;
	}

	public void setBtnCadastrarPessoa(JButton btnCadastrarPessoa) {
		this.btnCadastrarPessoa = btnCadastrarPessoa;
	}

	public JButton getBtnConsultarPessoa() {
		return btnConsultarPessoa;
	}

	public void setBtnConsultarPessoa(JButton btnConsultarPessoa) {
		this.btnConsultarPessoa = btnConsultarPessoa;
	}
}
