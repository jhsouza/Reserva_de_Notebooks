package view;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controller.InstituicaoController;
import dao.InstituicaoDAO;
import model.Instituicao;
import model.InstituicaoTableModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.border.EtchedBorder;

import java.awt.Color;

public class ConsultaInstituicaoUI extends JInternalFrame {
	private JTable jtListarInst;
	private InstituicaoController instControl = new InstituicaoController();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaInstituicaoUI frame = new ConsultaInstituicaoUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsultaInstituicaoUI() {
		setTitle("Consulta de Instituiçoes");
		setBounds(100, 100, 520, 314);
		setLocation(200, 40);

		JScrollPane jspTabelaInst = new JScrollPane();

		JButton btnNovainsti = new JButton("Nova Instituição");
		btnNovainsti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastrarInstituicaoUI novaInst = new CadastrarInstituicaoUI();
				getParent().add(novaInst, 0);
				novaInst.setVisible(true);
			}
		});

		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int linhaSelecionada = jtListarInst.getSelectedRow();
				try {

					Instituicao inst = new InstituicaoTableModel(InstituicaoDAO.getInstancia().ListaInst)
							.get(linhaSelecionada);
					CadastrarInstituicaoUI InstUI = new CadastrarInstituicaoUI();
					InstUI.setInstituicaoParaEdicao(inst);
					getParent().add(InstUI, 0);
					InstUI.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		});

		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int linhaSelecionada = jtListarInst.getSelectedRow();
				Instituicao instituicao = new InstituicaoTableModel(InstituicaoDAO.getInstancia().listarinstituicao()).get(linhaSelecionada);
				new InstituicaoController().remover(instituicao);

				List<Instituicao> listainst = instControl.listarTodos();
				InstituicaoTableModel modelinst = new InstituicaoTableModel(listainst);
				jtListarInst.setModel(modelinst);
			}
		});

		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PrincipalUI.getInstance().ativaBotoes(2);
				dispose();
			}
		});

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout
				.setHorizontalGroup(
						groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(
										groupLayout.createSequentialGroup().addContainerGap()
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(jspTabelaInst, GroupLayout.DEFAULT_SIZE, 481,
																Short.MAX_VALUE)
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(btnNovainsti)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(btnEditar)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(btnExcluir)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(btnFechar)))
												.addContainerGap()));
		groupLayout
				.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(20)
								.addComponent(jspTabelaInst, GroupLayout.PREFERRED_SIZE, 219,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(btnNovainsti)
										.addComponent(btnEditar).addComponent(btnExcluir).addComponent(btnFechar))
								.addContainerGap(15, Short.MAX_VALUE)));

		getContentPane().setLayout(groupLayout);
		
		jtListarInst = new JTable();
		InstituicaoTableModel modelinst = new InstituicaoTableModel(new InstituicaoController().listarTodos());
		jtListarInst.setModel(modelinst);
		jspTabelaInst.setViewportView(jtListarInst);

	}
}
