package interfaceModel;


public interface ITableModel {
	
	public int getRowCount();

	public int getColumnCount();

	public String getColumnName(int column);

	public Object getValueAt(int row, int column);

	public void setValueAt(Object aValue, int rowIndex, int columnIndex);

	public Class<?> getColumnClass(int columnIndex);

	public boolean isCellEditable(int rowIndex, int columnIndex);
	

}
