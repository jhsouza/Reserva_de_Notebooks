package model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import interfaceModel.ITableModel;

public class NotebookTableModel extends AbstractTableModel implements ITableModel {
	
	private static final long serialVersionUID = 1L;
	private static final int COL_COD_PATRIMONIO = 0;
	private static final int COL_DESCRICAO = 1;
	private static final int COL_SPECS = 2;
	private static final int COL_INSTITUICAO = 3;

	private List<Notebook> valores;

	public NotebookTableModel(List<Notebook> valores) {
		this.valores = new ArrayList<Notebook>(valores);
	}

	public int getRowCount() {

		return valores.size();
	}

	public int getColumnCount() {

		return 4;
	}

	public String getColumnName(int column) {

		if (column == COL_COD_PATRIMONIO)
			return "Código do Patrimônio";
		if (column == COL_DESCRICAO)
			return "Descrição";
		if (column == COL_SPECS)
			return "Especificações";
		if (column == COL_INSTITUICAO)
			return "Instituição";
		return "";
	}

	public Object getValueAt(int row, int column) {

		Notebook note = valores.get(row);
		if (column == COL_COD_PATRIMONIO)
			return note.getCodigo();
		else if (column == COL_DESCRICAO)
			return note.getDescricao();
		else if (column == COL_SPECS)
			return note.getSpecs();
		else if (column == COL_INSTITUICAO)
			return note.getInstituicao().getNome();

		return "";
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Notebook note = valores.get(rowIndex);

		if (columnIndex == COL_COD_PATRIMONIO)
			note.setCodigo(aValue.toString());
		else if (columnIndex == COL_DESCRICAO)
			note.setDescricao(aValue.toString());
		else if (columnIndex == COL_SPECS)
			note.setSpecs(aValue.toString());
		else if (columnIndex == COL_INSTITUICAO)
			note.getInstituicao().setNome(aValue.toString());

	}

	public Class<?> getColumnClass(int columnIndex) {

		return String.class;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	public Notebook get(int row) {
		return valores.get(row);
	}
}
