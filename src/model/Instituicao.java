package model;

public class Instituicao {

	private int id;
	private String nome;
	private String rsocial;
	private long cnpj;
	private String logradouro;
	private String cep;
	private String cidade;
	private String estado;
	private String uf;

	public Instituicao() {

	}

	public Instituicao(int id, String nome, String rsocial, long cnpj, String logradouro, String cep, String cidade,
			String estado, String uf) {

		this.id = id;
		this.nome = nome;
		this.rsocial = rsocial;
		this.cnpj = cnpj;
		this.logradouro = logradouro;
		this.cep = cep;
		this.cidade = cidade;
		this.estado = estado;
		this.uf = uf;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRsocial() {
		return rsocial;
	}

	public void setRsocial(String rsocial) {
		this.rsocial = rsocial;
	}

	public long getCnpj() {
		return cnpj;
	}

	public void setCnpj(long l) {
		this.cnpj = l;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}
	
	public String toString(){
		return nome;
	}

}