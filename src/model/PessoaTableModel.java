package model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import interfaceModel.ITableModel;

public class PessoaTableModel extends AbstractTableModel implements ITableModel {

	private static final long serialVersionUID = 1L;
	private static final int COL_NOME = 0;
	private static final int COL_CPF = 1;
	private static final int COL_MATRICULA = 2;
	private static final int COL_PROFESSOR = 3;
	private static final int COL_INSTITUICAO = 4;

	private List<Pessoa> valores;

	public PessoaTableModel(List<Pessoa> valores) {
		this.valores = new ArrayList<Pessoa>(valores);
	}

	public int getRowCount() {

		return valores.size();
	}

	public int getColumnCount() {

		return 5;
	}

	public String getColumnName(int column) {

		if (column == COL_NOME)
			return "Nome";
		if (column == COL_CPF)
			return "CPF";
		if (column == COL_MATRICULA)
			return "Matricula";
		if (column == COL_PROFESSOR)
			return "Professor";
		if (column == COL_INSTITUICAO)
			return "Instituição";
		return "";
	}

	public Object getValueAt(int row, int column) {
		Pessoa pessoa = valores.get(row);
		if (column == COL_NOME)
			return pessoa.getNome();
		else if (column == COL_CPF)
			return pessoa.getCpf();
		else if (column == COL_MATRICULA)
			return pessoa.getMatricula();
		else if (column == COL_PROFESSOR)
			return pessoa.getProfessor();
		else if (column == COL_INSTITUICAO)
			return pessoa.getInstituicao().getNome();
		return "";
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Pessoa pessoa = valores.get(rowIndex);
		if (columnIndex == COL_NOME)
			pessoa.setNome(aValue.toString());
		else if (columnIndex == COL_CPF)
			pessoa.setCpf(aValue.toString());
		else if (columnIndex == COL_MATRICULA)
			pessoa.setMatricula(aValue.toString());
		else if (columnIndex == COL_PROFESSOR)
			pessoa.setProfessor(aValue.toString());
		else if (columnIndex == COL_INSTITUICAO)
			pessoa.getInstituicao().setNome(aValue.toString());
	}

	public Class<?> getColumnClass(int columnIndex) {

		return String.class;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {

		return true;
	}

	public Pessoa get(int row) {
		return valores.get(row);
	}
}
