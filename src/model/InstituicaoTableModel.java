package model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class InstituicaoTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private static final int COL_NOME = 0;
	private static final int COL_RAZAOSOCIAL = 1;
	private static final int COL_CNPJ = 2;
	private static final int COL_LOGRADOURO = 3;
	private static final int COL_CEP = 4;
	private static final int COL_CIDADE = 5;
	private static final int COL_ESTADO = 6;
	private static final int COL_UF = 7;

	private List<Instituicao> valores;

	public InstituicaoTableModel(List<Instituicao> valores) {
		this.valores = new ArrayList<Instituicao>(valores);
	}

	public int getRowCount() {

		return valores.size();
	}

	public int getColumnCount() {

		return 8;
	}

	public String getColumnName(int column) {

		if (column == COL_NOME)
			return "Nome";
		if (column == COL_RAZAOSOCIAL)
			return "Raz�o Social";
		if (column == COL_CNPJ)
			return "CNPJ";
		if (column == COL_LOGRADOURO)
			return "logradouro";
		if (column == COL_CEP)
			return "CEP";
		if (column == COL_CIDADE)
			return "Cidade";
		if (column == COL_ESTADO)
			return "Estado";
		if (column == COL_UF)
			return "UF";

		return "";
	}

	public Object getValueAt(int row, int column) {

		Instituicao inst = valores.get(row);
		if (column == COL_NOME)
			return inst.getNome();
		else if (column == COL_RAZAOSOCIAL)
			return inst.getRsocial();
		else if (column == COL_CNPJ)
			return inst.getCnpj();
		else if (column == COL_LOGRADOURO)
			return inst.getLogradouro();
		else if (column == COL_CEP)
			return inst.getCep();
		else if (column == COL_CIDADE)
			return inst.getCidade();
		else if (column == COL_ESTADO)
			return inst.getEstado();
		else if (column == COL_UF)
			return inst.getUf();

		return "";
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Instituicao inst = valores.get(rowIndex);

		if (columnIndex == COL_NOME)
			inst.setNome(aValue.toString());
		else if (columnIndex == COL_RAZAOSOCIAL)
			inst.setRsocial(aValue.toString());
		else if (columnIndex == COL_CNPJ)
			inst.setCnpj(Integer.parseInt(aValue.toString()));
		else if (columnIndex == COL_LOGRADOURO)
			inst.setLogradouro(aValue.toString());
		else if (columnIndex == COL_CEP)
			inst.setRsocial(aValue.toString());
		else if (columnIndex == COL_CIDADE)
			inst.setCidade(aValue.toString());
		else if (columnIndex == COL_ESTADO)
			inst.setEstado(aValue.toString());
		else if (columnIndex == COL_UF)
			inst.setUf(aValue.toString());

	}

	public Class<?> getColumnClass(int columnIndex) {

		return String.class;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	public Instituicao get(int row) {
		return valores.get(row);
	}
}
