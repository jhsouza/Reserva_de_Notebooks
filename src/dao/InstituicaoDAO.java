package dao;

import java.security.KeyStore.ProtectionParameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import intefaceDAO.IInstituicaoDAO;
import interfaceController.IInstituicaoController;
import model.Instituicao;
import util.ConnectionUtil;

public class InstituicaoDAO implements IInstituicaoDAO {

	private static InstituicaoDAO instancia;
	public ArrayList<Instituicao> ListaInst;
	private Connection con = ConnectionUtil.getConnection();

	public static InstituicaoDAO getInstancia() {
		if (instancia == null) {
			instancia = new InstituicaoDAO();
		}
		return instancia;
	}

	@Override
	public void salvar(Instituicao instituicao) {
		try {
			String sql = "insert into instituicao (nome, rsocial, cnpj, logradouro, cep, cidade, estado, uf) values (?,?,?,?,?,?,?,?);";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, instituicao.getNome());
			pstmt.setString(2, instituicao.getRsocial());
			pstmt.setLong(3, instituicao.getCnpj());
			pstmt.setString(4, instituicao.getLogradouro());
			pstmt.setString(5, instituicao.getCep());
			pstmt.setString(6, instituicao.getCidade());
			pstmt.setString(7, instituicao.getEstado());
			pstmt.setString(8, instituicao.getUf());

			pstmt.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Instituicao> listarinstituicao() {

		try {
			ListaInst = new ArrayList<>();
			Statement stmt = con.createStatement();
			String sql = "select * from instituicao";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Instituicao inst = new Instituicao();
				inst.setId(rs.getInt("instituicao_id"));
				inst.setNome(rs.getString("nome"));
				inst.setRsocial(rs.getString("rsocial"));
				inst.setCnpj(rs.getInt("CNPJ"));
				inst.setLogradouro(rs.getString("Logradouro"));
				inst.setCep(rs.getString("CEP"));
				inst.setCidade(rs.getString("Cidade"));
				inst.setEstado(rs.getString("Estado"));
				inst.setUf(rs.getString("UF"));
				ListaInst.add(inst);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ListaInst;
	}

	@Override
	public void editar(Instituicao instituicao) {
		try {

			String sql = "update instituicao set nome = ?,rsocial = ?,cnpj = ?, logradouro = ?,cep = ?,cidade = ?,estado = ?,uf = ? where instituicao_id = ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, instituicao.getNome());
			pstmt.setString(2, instituicao.getRsocial());
			pstmt.setLong(3, instituicao.getCnpj());
			pstmt.setString(4, instituicao.getLogradouro());
			pstmt.setString(5, instituicao.getCep());
			pstmt.setString(6, instituicao.getCidade());
			pstmt.setString(7, instituicao.getEstado());
			pstmt.setString(8, instituicao.getUf());
			pstmt.setInt(9, instituicao.getId());
			pstmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void excluir(int id) {

		try {
			String sql = "delete from instituicao where instituicao_id = ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
