package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.fabric.xmlrpc.Client;

import intefaceDAO.IPessoaDAO;
import util.ConnectionUtil;
import model.Instituicao;
import model.Pessoa;

public class PessoaDAO implements IPessoaDAO{

	private static PessoaDAO instancia;
	public ArrayList<Pessoa> listaPessoas;
	private Connection con = ConnectionUtil.getConnection();
	
	
	public static PessoaDAO obterInstancia(){
		if (instancia == null){
			instancia = new PessoaDAO();
		}
		return instancia;
	}
	
	@Override
	public void salvar(Pessoa pessoa){
		try{
		String sql = "insert into pessoa (nome, cpf,matricula,professor,instituicao_id) values (?,?,?,?,?)";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, pessoa.getNome());
		pstmt.setString(2,pessoa.getCpf());
		pstmt.setString(3,pessoa.getMatricula());
		pstmt.setString(4, pessoa.getProfessor());
		pstmt.setInt(5, pessoa.getInstituicao().getId());
		pstmt.execute();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Pessoa> listarPessoas(){
		try{
			listaPessoas = new ArrayList<>();
		Statement stmt = con.createStatement();
		String sql = "select p.id_pessoa,p.nome as nome_pessoa,cpf,matricula,professor,p.instituicao_id,i.nome as nome_inst from pessoa p join instituicao i on p.instituicao_id  = i.instituicao_id  order by p.nome";
		ResultSet rs = stmt.executeQuery(sql);
		while(rs.next()){
			Pessoa pessoa = new Pessoa();
			Instituicao inst = new Instituicao();
			pessoa.setId(rs.getInt("id_pessoa"));
			pessoa.setNome(rs.getString("nome_pessoa"));
			pessoa.setCpf(rs.getString("cpf"));
			pessoa.setMatricula(rs.getString("matricula"));
			pessoa.setProfessor(rs.getString("professor"));		
			inst.setId(rs.getInt("instituicao_id"));
			inst.setNome(rs.getString("nome_inst"));
			pessoa.setInstituicao(inst);
			listaPessoas.add(pessoa);
		}
		
		
		}catch(SQLException e){
			e.printStackTrace();
		}
		return listaPessoas;
	}
	
	@Override
	public void editar(Pessoa pessoa){
		try{
		String sql = "update pessoa set nome = ?, cpf = ?, matricula = ?, professor = ?,instituicao_id = ? where id_pessoa = ?";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, pessoa.getNome());
		pstmt.setString(2,pessoa.getCpf());
		pstmt.setString(3,pessoa.getMatricula());		
		pstmt.setString(4, pessoa.getProfessor());
		pstmt.setInt(5, pessoa.getInstituicao().getId());
		pstmt.setInt(6, pessoa.getId());
		pstmt.execute();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void excluir(int id){
		try{
		String sql = "delete from pessoa where id_pessoa = ?";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, id);
		pstmt.execute();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	
	public List<Pessoa> pesquisar(String nome){
		try{
			String sql = "select p.id_pessoa,p.nome as nome_pessoa,cpf,matricula,professor,p.instituicao_id"
					+ " from pessoa p join instituicao i on p.instituicao_id = i.instituicao_id where nome like ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, "%"+nome+"%");
			ResultSet rs = pstmt.executeQuery();
			listaPessoas = new ArrayList<>();
			while(rs.next()){
				Pessoa pessoa = new Pessoa();
				Instituicao inst = new Instituicao();
				pessoa.setId(rs.getInt("id_pessoa"));
				pessoa.setNome(rs.getString("nome_pessoa"));
				pessoa.setCpf(rs.getString("cpf"));
				pessoa.setMatricula(rs.getString("matricula"));
				pessoa.setProfessor(rs.getString("professor"));
				inst.setId(rs.getInt("instituicao"));
				pessoa.setInstituicao(inst);
				listaPessoas.add(pessoa);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return listaPessoas;
	}
	
}
