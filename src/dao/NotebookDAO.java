package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import intefaceDAO.INotebookDAO;
import model.Instituicao;
import model.Notebook;
import util.ConnectionUtil;

public class NotebookDAO implements INotebookDAO {
	private static NotebookDAO instancia;
	public ArrayList<Notebook> listaNotebooks = new ArrayList<>();
	private Connection con = ConnectionUtil.getConnection();
	
	public static NotebookDAO obterInstancia(){
		if (instancia == null) {
			instancia = new NotebookDAO();
		}
		return instancia;
	}

	@Override
	public void salvar(Notebook notebook) {
		try {
			String sql = "insert into notebook (codigo_notebook, descricao, especificacao, instituicao_id) values (?,?,?,?);";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, notebook.getCodigo());
			pstmt.setString(2, notebook.getDescricao());
			pstmt.setString(3, notebook.getSpecs());
			pstmt.setInt(4, notebook.getInstituicao().getId());
			pstmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<Notebook> listarNotebook() {
		try{
			listaNotebooks = new ArrayList<>();
			Statement stmt = con.createStatement();
			String sql = "select n.id_notebook,n.codigo_notebook,n.descricao, n.especificacao,i.instituicao_id,i.nome as nome_inst from notebook n join instituicao i on n.instituicao_id = i.instituicao_id  order by n.codigo_notebook";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
				Notebook notebook = new Notebook();
				Instituicao inst = new Instituicao();
				notebook.setId(rs.getInt("id_notebook"));
				notebook.setCodigo(rs.getString("codigo_notebook"));
				notebook.setDescricao(rs.getString("descricao"));
				notebook.setSpecs(rs.getString("especificacao"));
				inst.setId(rs.getInt("instituicao_id"));
				inst.setNome(rs.getString("nome_inst"));
				notebook.setInstituicao(inst);
				listaNotebooks.add(notebook);
			}		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaNotebooks;
	}

	@Override
	public void editar(Notebook notebook) {
		try{
			String sql = "update notebook set codigo_notebook = ?, descricao = ?, especificacao = ?, instituicao_id = ? where id_notebook = ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, notebook.getCodigo());
			pstmt.setString(2, notebook.getDescricao());
			pstmt.setString(3, notebook.getSpecs());
			pstmt.setInt(4, notebook.getInstituicao().getId());
			pstmt.setInt(5, notebook.getId());
			pstmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void excluir(int id) {
		try{
			String sql = "delete from notebook where id_notebook = ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
}
