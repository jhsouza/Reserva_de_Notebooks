package ferramentas;

import java.awt.Color;

import javax.swing.JFrame;

public class FerramentasFrame extends JFrame{
	
	
	public void aplicarTransparencia(JFrame frame){
		
		frame.setUndecorated(true);
		frame.setBackground(new Color(0 ,0 ,100, 0));
		
		
	}
	
	public void centralizarFrame(JFrame frame){
		
		frame.setLocationRelativeTo(null);
		
	}

}
