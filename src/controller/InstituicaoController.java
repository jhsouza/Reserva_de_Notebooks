package controller;

import java.util.List;
import dao.InstituicaoDAO;
import interfaceController.IInstituicaoController;
import model.Instituicao;

public class InstituicaoController implements IInstituicaoController {

	@Override
	public void salvar(Instituicao instituicao) {
		InstituicaoDAO.getInstancia().salvar(instituicao);
	}

	@Override
	public void editar(Instituicao instituicao) {
		InstituicaoDAO.getInstancia().editar(instituicao);
	}

	@Override
	public void remover(Instituicao instituicao) {
		InstituicaoDAO.getInstancia().excluir(instituicao.getId());
	}

	@Override
	public List<Instituicao> listarTodos() {
		return InstituicaoDAO.getInstancia().listarinstituicao();
	}

}