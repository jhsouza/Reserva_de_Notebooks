package controller;

import java.util.InputMismatchException;
import java.util.List;

import javax.swing.JOptionPane;

import interfaceController.IPessoaController;
import dao.PessoaDAO;
import model.Pessoa;

public class PessoaController implements IPessoaController {

	@Override
	public void salvar(Pessoa pessoa) throws Exception {
		// TODO Auto-generated method stub
		
		if(pessoa.getNome().trim().equals("") || 
				pessoa.getNome().length() < 5 ||
				pessoa.getNome().length() > 60){
			throw new Exception("Nome inv�lido");
		}
		if(pessoa.getCpf().trim().equals("") || 
				!isCPF(pessoa.getCpf()) ){
			throw new Exception("CPF inv�lido");
		}
		for(int i= 0;i<listarTodos().size();i++){
			if(listarTodos().get(i).getCpf().equals(pessoa.getCpf())){
				throw new Exception("CPF j� cadastrado");
			}
		}	
		if(pessoa.getMatricula().trim().equals("")){
			if(pessoa.getProfessor().equals("sim") && pessoa.getMatricula().length()!=6){
				throw new Exception("Matricula inv�lido");	
			}else if(pessoa.getProfessor().equals("nao") && pessoa.getMatricula().length()>=7) {
				throw new Exception("Matricula inv�lido");
			}
		}
		
		isMatricula(pessoa.getMatricula());
		for(int i= 0;i<listarTodos().size();i++){
			if(listarTodos().get(i).getMatricula().equals(pessoa.getCpf())){
				throw new Exception("CPF j� cadastrado");
			}
		}
		if(pessoa.getInstituicao() == null){
			throw new Exception("Selecione uma institui��o");
		}
		PessoaDAO.obterInstancia().salvar(pessoa);
	}

	@Override
	public void editar(Pessoa pessoa) throws Exception{
		// TODO Auto-generated method stub		
		if(!(pessoa.getId() > 0)){
			throw new Exception("Selecione um Aluno/Professor");
		}
		if(pessoa.getNome().trim().equals("") || 
				pessoa.getNome().length() < 5 ||
				pessoa.getNome().length() > 80){
			throw new Exception("Nome inv�lido");
		}
		if(pessoa.getCpf().trim().equals("") || 
				!isCPF(pessoa.getCpf()) ){
			throw new Exception("CPF inv�lido");
		}		
		if(pessoa.getMatricula().trim().equals("")){
			if(pessoa.getProfessor().equals("sim") && pessoa.getMatricula().length()!=6){
				throw new Exception("Matricula inv�lido");	
			}else if(pessoa.getProfessor().equals("nao") && pessoa.getMatricula().length()>=7) {
				throw new Exception("Matricula inv�lido");
			}
		}
		if(pessoa.getInstituicao() == null){
			throw new Exception("Selecione uma institui��o");
		}
		PessoaDAO.obterInstancia().editar(pessoa);
		
	}

	@Override
	public void remover(Pessoa pessoa) throws Exception{
		// TODO Auto-generated method stub		
		if(!(pessoa.getId() > 0)){
			throw new Exception("Selecione um Aluno/Professor");
		}
		PessoaDAO.obterInstancia().excluir(pessoa.getId());
		
	}

	@Override
	public List<Pessoa> listarTodos() {
		// TODO Auto-generated method stub
		return PessoaDAO.obterInstancia().listarPessoas();
	}
	
	@Override
	public List<Pessoa> pesquisar(String nome) {
		// TODO Auto-generated method stub
		return PessoaDAO.obterInstancia().pesquisar(nome.trim());
	}
	
	private void isMatricula(String matricula) throws Exception{
		
		try{
			long aux;
			aux = Long.parseLong(matricula);
		}catch(NumberFormatException e){
			throw new Exception("Caracteres invalidos");
		}
		
		
	}
	
	private boolean isCPF(String CPF) {
		// considera-se erro CPF's formados por uma sequencia de numeros iguais
		if (CPF.equals("00000000000") || CPF.equals("11111111111") ||
				CPF.equals("22222222222") || CPF.equals("33333333333") ||
				CPF.equals("44444444444") || CPF.equals("55555555555") ||
				CPF.equals("66666666666") || CPF.equals("77777777777") ||
				CPF.equals("88888888888") || CPF.equals("99999999999") ||
				(CPF.length() != 11))
			return(false);

		char dig10, dig11;
		int sm, i, r, num, peso;

		// "try" - protege o codigo para eventuais erros de conversao de tipo (int)
		try {
			// Calculo do 1o. Digito Verificador
			sm = 0;
			peso = 10;
			for (i=0; i<9; i++) {              
				// converte o i-esimo caractere do CPF em um numero:
				// por exemplo, transforma o caractere '0' no inteiro 0         
				// (48 eh a posicao de '0' na tabela ASCII)         
				num = (int)(CPF.charAt(i) - 48); 
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11))
				dig10 = '0';
			else dig10 = (char)(r + 48); // converte no respectivo caractere numerico

			// Calculo do 2o. Digito Verificador
			sm = 0;
			peso = 11;
			for(i=0; i<10; i++) {
				num = (int)(CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11))
				dig11 = '0';
			else dig11 = (char)(r + 48);

			// Verifica se os digitos calculados conferem com os digitos informados.
			if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
				return(true);
			else return(false);
		} catch (InputMismatchException erro) {
			return(false);
		}
	}

	
	
	
}
