package controller;

import java.util.List;

import dao.NotebookDAO;
import interfaceController.INotebookController;
import model.Notebook;

public class NotebookController implements INotebookController {

	public void salvar(Notebook notebook) throws Exception {
		if (!(notebook.getCodigo().trim().length() == 7)) {
			throw new Exception("C�digo de patrim�nio inv�lido");
		}
		
		if (notebook.getInstituicao() == null) {
			throw new Exception("Seleciona uma institui��o");
		}	
		
		NotebookDAO.obterInstancia().salvar(notebook);
	}

	public void editar(Notebook notebook) throws Exception {
		if (!(notebook.getCodigo().trim().length() ==7)) {
			throw new Exception("C�digo de patrim�nio inv�lido");
		}
		
		if (notebook.getInstituicao() == null) {
			throw new Exception("Seleciona uma institui��o");
		}	

		NotebookDAO.obterInstancia().editar(notebook);

	}

	public void remover(Notebook notebook) throws Exception {
		if (!(notebook.getId() > 0)) {
			throw new Exception("Selecione um notebook");
		}
		
		NotebookDAO.obterInstancia().excluir(notebook.getId());
		
	}

	public List<Notebook> listarTodos() {
		return NotebookDAO.obterInstancia().listarNotebook();

	}


}
