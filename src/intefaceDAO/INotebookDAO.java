package intefaceDAO;

import java.util.List;

import model.Notebook;

public interface INotebookDAO {
	public void salvar(Notebook notebook);
	
	public List<Notebook> listarNotebook();
	
	public void editar(Notebook notebook);
	
	public void excluir(int id);
}
