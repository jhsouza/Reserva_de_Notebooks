package intefaceDAO;

import java.util.List;

import model.Pessoa;

public interface IPessoaDAO {
	
	
	public void salvar(Pessoa pessoa);
	
	public List<Pessoa> listarPessoas();
	
	public void editar(Pessoa pessoa);
	
	public void excluir(int id);

}
