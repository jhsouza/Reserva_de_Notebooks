package intefaceDAO;

import java.util.List;

import model.Instituicao;


public interface IInstituicaoDAO {
	
	
	public void salvar(Instituicao instituicao);
	
	public List<Instituicao> listarinstituicao();
	
	public void editar(Instituicao instituicao);
	
	public void excluir(int id);

}
