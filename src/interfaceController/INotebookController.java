package interfaceController;

import java.util.List;
import model.Notebook;

public interface INotebookController {
	public void salvar(Notebook notebook) throws Exception;
	
	public void editar(Notebook notebook) throws Exception;
	
	public void remover(Notebook notebook) throws Exception;
	
	public List<Notebook> listarTodos();
}
