package interfaceController;

import java.util.List;

import model.Pessoa;


	
public interface IPessoaController {
	
	
	public void salvar(Pessoa pessoa) throws Exception;
	
	public void editar(Pessoa pessoa) throws Exception;
	
	public void remover(Pessoa pessoa) throws Exception;
	
	public List<Pessoa> listarTodos();
	
	public List<Pessoa> pesquisar(String nome);

}
