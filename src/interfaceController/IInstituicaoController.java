package interfaceController;

import java.util.List;

import model.Instituicao;



	
public interface IInstituicaoController {
	
	
	public void salvar(Instituicao instituicao);
	
	public void editar(Instituicao instituicao);
	
	public void remover(Instituicao instituicao);
	
	public List<Instituicao> listarTodos();

}
